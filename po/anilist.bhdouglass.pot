# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR THE PACKAGE'S COPYRIGHT HOLDER
# This file is distributed under the same license as the anilist.bhdouglass package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: anilist.bhdouglass\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2020-08-14 04:45+0000\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=CHARSET\n"
"Content-Transfer-Encoding: 8bit\n"

#: ../qml/Main.qml:149
msgid "Home"
msgstr ""

#: ../qml/Main.qml:154
msgid "Anime List"
msgstr ""

#: ../qml/Main.qml:159
msgid "Manga List"
msgstr ""

#: ../qml/Main.qml:164
msgid "Search"
msgstr ""

#: ../qml/Main.qml:169 ../qml/Main.qml:195
msgid "Settings"
msgstr ""

#: ../qml/Main.qml:209
msgid "Username"
msgstr ""

#: ../qml/Main.qml:214
msgid "Enter your username for the bottom links"
msgstr ""

#: ../qml/Main.qml:228
msgid "Default Anime List"
msgstr ""

#: ../qml/Main.qml:236 ../qml/Main.qml:255
msgid "All"
msgstr ""

#: ../qml/Main.qml:237
msgid "Watching"
msgstr ""

#: ../qml/Main.qml:238
msgid "Plan to watch"
msgstr ""

#: ../qml/Main.qml:239 ../qml/Main.qml:258
msgid "Completed"
msgstr ""

#: ../qml/Main.qml:240
msgid "Rewatching"
msgstr ""

#: ../qml/Main.qml:241 ../qml/Main.qml:260
msgid "Paused"
msgstr ""

#: ../qml/Main.qml:242 ../qml/Main.qml:261
msgid "Dropped"
msgstr ""

#: ../qml/Main.qml:247
msgid "Default Manga List"
msgstr ""

#: ../qml/Main.qml:256
msgid "Reading"
msgstr ""

#: ../qml/Main.qml:257
msgid "Plan to read"
msgstr ""

#: ../qml/Main.qml:259
msgid "Rereading"
msgstr ""

#: ../qml/Main.qml:269
msgid "OK"
msgstr ""

#: anilist.desktop.in.h:1
msgid "AniList"
msgstr ""
