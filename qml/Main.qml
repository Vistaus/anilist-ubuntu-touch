import QtQuick 2.9
import QtQuick.Layouts 1.3
import Qt.labs.settings 1.0
import QtQuick.Controls 2.2
import QtWebEngine 1.7
import Morph.Web 0.1

import "Components" as Components

ApplicationWindow {
    id: root

    visible: true
    width: units.gu(50)
    height: units.gu(75)

    Settings {
        id: settings
        property string lastUrl: 'https://anilist.co'
        property string username
        property int defaultAnimeList: 0
        property int defaultMangaList: 0
    }

    WebContext {
        id: webcontext
        userAgent: 'Mozilla/5.0 (Linux; Android 8.0; Nexus 5) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3325.151 Mobile Safari/537.36 Ubuntu Touch Webapp'
        offTheRecord: false

        userScripts: [
            WebEngineScript {
                injectionPoint: WebEngineScript.DocumentCreation
                sourceUrl: Qt.resolvedUrl('inject.js')
                worldId: WebEngineScript.MainWorld
            }
        ]
    }

    WebView {
        id: webview
        anchors {
            top: parent.top
            right: parent.right
            left: parent.left
            bottom: nav.top
        }

        context: webcontext
        url: settings.lastUrl
        onUrlChanged: {
            var strUrl = url.toString();
            console.log(strUrl);
            if (settings.lastUrl != strUrl && strUrl.match('(http|https)://anilist.co/(.*)')) {
                settings.lastUrl = strUrl;
            }

            var match = strUrl.match('^https:\/\/anilist.co\/user\/([^\/]+)\/');
            if (match && !settings.username) {
                settings.username = match[1];
            }
        }

        function navigationRequestedDelegate(request) {
            var url = request.url.toString();

            if (!url.match('(http|https)://anilist.co/(.*)') && request.isMainFrame) {
                Qt.openUrlExternally(url);
                request.action = WebEngineNavigationRequest.IgnoreRequest;
            }
        }
    }

    ProgressBar {
        height: units.dp(3)
        anchors {
            left: parent.left
            right: parent.right
            top: parent.top
        }

        value: (webview.loadProgress / 100)
        visible: (webview.loading && !webview.lastLoadStopped)
    }

    // TODO migrate to QQC2
    Components.BottomNavigationBar {
        id: nav

        function animeList() {
            if (settings.username) {
                var url = 'https://anilist.co/user/' + settings.username + '/animelist';
                if (settings.defaultAnimeList == 1) {
                    url += '/Watching';
                }
                else if (settings.defaultAnimeList == 2) {
                    url += '/Planning';
                }
                else if (settings.defaultAnimeList == 3) {
                    url += '/Completed';
                }
                else if (settings.defaultAnimeList == 4) {
                    url += '/Rewatching';
                }
                else if (settings.defaultAnimeList == 5) {
                    url += '/Paused';
                }
                else if (settings.defaultAnimeList == 6) {
                    url += '/Dropped';
                }

                return url;
            }

            return null;
        }

        function mangaList() {
            if (settings.username) {
                var url = 'https://anilist.co/user/' + settings.username + '/mangalist';

                if (settings.defaultMangaList == 1) {
                    url += '/Reading';
                }
                else if (settings.defaultMangaList == 2) {
                    url += '/Planning';
                }
                else if (settings.defaultMangaList == 3) {
                    url += '/Completed';
                }
                else if (settings.defaultMangaList == 4) {
                    url += '/Rewatching';
                }
                else if (settings.defaultMangaList == 5) {
                    url += '/Paused';
                }
                else if (settings.defaultMangaList == 6) {
                    url += '/Dropped';
                }

                return url;
            }

            return null;
        }

        selectedIndex: -1 // Don't show any items as active
        model: [
            {
                'name': i18n.tr('Home'),
                'iconName': 'home',
                'url': 'https://anilist.co',
            },
            {
                'name': i18n.tr('Anime List'),
                'iconName': 'view-list-symbolic',
                'url': animeList(),
            },
            {
                'name': i18n.tr('Manga List'),
                'iconName': 'view-list-symbolic',
                'url': mangaList(),
            },
            {
                'name': i18n.tr('Search'),
                'iconName': 'toolkit_input-search',
                'url': 'https://anilist.co/search/anime',
            },
            {
                'name': i18n.tr('Settings'),
                'iconName': 'settings',
                'url': null,
            }
        ]

        onTabThumbClicked: {
            if (model[index].url) {
                webview.url = model[index].url;
            }
            else {
                settingsDialog.open();
            }
        }
    }

    Dialog {
        id: settingsDialog

        width: parent.width - units.gu(10)

        x: (parent.width - width) / 2
        y: (parent.height - height) / 2
        parent: ApplicationWindow.overlay

        modal: true
        title: i18n.tr('Settings')

        function save() {
            settings.username = user.text;
            settings.defaultAnimeList = defaultAnime.currentIndex;
            settings.defaultMangaList = defaultManga.currentIndex;
            settingsDialog.close();
        }

        ColumnLayout {
            spacing: units.gu(1)
            anchors.fill: parent

            Label {
                text: i18n.tr('Username')
            }

            Label {
                visible: !settings.username
                text: i18n.tr('Enter your username for the bottom links')
            }

            TextField {
                id: user
                Layout.fillWidth: true

                text: settings.username

                inputMethodHints: Qt.ImhNoPredictiveText | Qt.ImhNoAutoUppercase
                onAccepted: settingsDialog.save()
            }

            Label {
                text: i18n.tr('Default Anime List')
            }

            ComboBox {
                id: defaultAnime
                Layout.fillWidth: true

                model: [
                    i18n.tr('All'),
                    i18n.tr('Watching'),
                    i18n.tr('Plan to watch'),
                    i18n.tr('Completed'),
                    i18n.tr('Rewatching'),
                    i18n.tr('Paused'),
                    i18n.tr('Dropped'),
                ]
            }

            Label {
                text: i18n.tr('Default Manga List')
            }

            ComboBox {
                id: defaultManga
                Layout.fillWidth: true

                model: [
                    i18n.tr('All'),
                    i18n.tr('Reading'),
                    i18n.tr('Plan to read'),
                    i18n.tr('Completed'),
                    i18n.tr('Rereading'),
                    i18n.tr('Paused'),
                    i18n.tr('Dropped'),
                ]
            }

            Button {
                Layout.fillWidth: true
                Layout.topMargin: units.gu(1)
                Layout.bottomMargin: units.gu(1)
                text: i18n.tr('OK')

                onClicked: settingsDialog.save()
            }
        }
    }

    Connections {
        target: UriHandler
        onOpened: {
            webview.url = uris[0];
        }
    }

    Component.onCompleted: {
        if (Qt.application.arguments[1] && Qt.application.arguments[1].indexOf('anilist.co') >= 0) {
            webview.url = Qt.application.arguments[1];
        }
    }
}
